              *******Phorum Converter*******

This module converts Phorum DB data into drupal's DB using the forum and the
comments module. Since Phorum message's subject field is defined to be
varchar(255) and the appropriate field's on the drupal side are only varchar(64)
or varchar(128) therefore the install will set the comments table's subject
field, the node's table title's field and the node_revisions table's title
field to varchar(255).

To access the Phorum DB please add the login and URI information to the
settings.php with the 'phorum' associative key. For example:
$db_url = array(
  'default' => 'mysql://Drupal_username:Drupal_password@localhost/Drupal_databasename',
  'phorum'  => 'mysql://Phorum_username:Phorum_password@localhost/Phorum_databasename',
);


++Modules you will probably want:
BBCode
Quicktags
Smileys

+++features to add to converter in the future (things it doesn't do now)
buddylist migration
private messages migration
private forums migration (with dependency on forum_access or similar)
redirects from old boards to new boards

This module was developed by a top secret developer (who shall remain nameless) 
for pingVision, LLC (www.pingv.com), and packaged/contributed by Greg Knaddison (greggles).

Since Drupal and Phorum are continually evolving, this module is offered as a 
snapshot of something that can work now (for Drupal 4.7.x and Phorum 5.1.x). 
If you are attempting to do an import from or to other versions, you may need 
to revise the code. We are not in the position to support continuous development 
of this module, sorry, but we welcome input and/or interest in assuming this project.  
We are potentially available for paid projects supporting a migration 
using this module.

Send problems/questions/patches to: 	http://drupal.org/project/issues/phorum_converter


